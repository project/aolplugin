<?php
/**
 * @file
 * The template that renders the content of the widget.
 */

$api_name = $variables['api_name'];
$api_sid = $variables['api_sid'];

if ($api_name == NULL || $api_sid == NULL): ?>
  <div id="fivemin-plugin">
    <?php
    echo t('Please configure the plugin first.');
    ?>
  </div>
<?php else: ?>
  <div id="drag-plugin">Drag</div>
  <div id="fivemin-plugin" data-api="<?php echo $api_name; ?>" data-params="sid=<?php echo $api_sid; ?>"></div>
<?php endif; ?>
