Drupal.behaviors.aolPlugin = {
  attach: function(context, settings){
    jQuery('#aolplugin-container').draggable({
      handle: '#drag-plugin'
    });
    FIVEMIN.Plugins.Api.drupal = {
      getAddLocations: function() {
        return [{ "location": "default", "text": "Add to post", "className": "fivemin-addtopost" }];
      },
      getSuggestData: function () {
        var basename = jQuery("input[name=title]").val() != null ? jQuery("input[name=title]").val() : "";
        var title = jQuery("input[name=title]").val();
        var text = jQuery("textarea.text-full").val();
        var verticals = "";
        var tags = jQuery("input[name*=field_tags]").val();
        return { "body": text, "title": title, "verticals": verticals, "tags": tags, "basename":basename };
      },
      getDomain: function () {
        return window.location.host;
      },
      addContents: function(html, playerseed){
        var textarea = jQuery(".field-name-body").find(".text-full");
        if(textarea.length == 0) {
          return false;
        }

        if(typeof(CKEDITOR) != "undefined") {
          // check if ckeditor is loaded for the body field
          for(instance in CKEDITOR.instances) {
            if(instance == textarea.attr("id")) {
              // add the video to this instance
              CKEDITOR.instances[instance].insertHtml('<div>' + html + '<div class="aolplugin-clearfix"></div></div>');
              return false;
            }
          }
        }

        if (typeof(window.tinyMCE) != "undefined") {
          // try to get the tinyMCE instance on the body field
          var instance = window.tinyMCE.getInstanceById(textarea.attr("id"))
          if(typeof(instance) != "undefined") {
            // instance found, add the text and leave
            instance.execCommand('mceInsertContent', false, '<div>' + html + '<div class="aolplugin-clearfix"></div></div>');
            return false;
          }
        }

        insertAtPosition(textarea[0], html);
      }
    }

    jQuery("textarea").each(function(){
      this.onmouseup = this.onkeyup = function () {
        jQuery(this).attr("data-caret-position", getCaret(this));
      };
    });
  }
}

function getCaret(el) {
  if (el.selectionStart) {
    return el.selectionStart;
  }
  else if (document.selection) {
    el.focus();

    var r = document.selection.createRange();
    if (r === null) {
      return 0;
    }

    var re = el.createTextRange(), rc = re.duplicate();
    re.moveToBookmark(r.getBookmark());
    rc.setEndPoint("EndToStart", re);

    return rc.text.length;
  }
  return 0;
}

function insertAtPosition(el, text) {
  var oldText = jQuery(el).val();
  var pos = jQuery(el).attr("data-caret-position");
  var before = oldText.substr(0, pos);
  var after = oldText.substr(pos, text.length);

  var nl = "";
  var twoBefore = before.substr(-2);
  var oneBefore = before.substr(-1);
  var twoAfter = after.substr(0, 2);
  var oneAfter = after.substr(0, 1);

  // add correct number of new lines before
  if(twoBefore == '\n\n' || twoBefore == ''){
    nlBefore = '';
  }
  else if(oneBefore == '\n'){
    nlBefore = '\n';
  }
  else{
    nlBefore = '\n\n';
  }

  // add correct number of new lines after
  if(twoAfter == '\n\n'){
    nlAfter = '';
  }
  else if(oneAfter == '\n'){
    nlAfter = '\n';
  }
  else{
    nlAfter = '\n\n';
  }

  jQuery(el).val(before + nlBefore + text + nlAfter + after);
}
