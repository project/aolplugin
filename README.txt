Aol Video Plugin for Drupal 7.x

TABLE OF CONTENTS
=================
 1. Introduction
 2. Installation
 3. Usage


INTRODUCTION
------------
As part of the efforts to facilitate the work process of the editors, we have
created the AOL On Plugin. The idea is to provide editors with tools for
embedding videos that best fit the article using the following features:
 - Search: Search the vast AOL Video Library
 - Browse: Browse videos by Studio, category or sub-categories
 - Semantically Matched Suggestions: Receive suggestions for semantically
   matched videos for the article/post
 - Editors Room selections: View your selections from the Editors Room
 - Playlist: Create and grab a whole playlist to your article/post

INSTALLATION
------------
After enabling the module, configure it at admin/settings/aolplugin:
 - Have the Aol On widget show up on the node add/edit pages by checking
   the "Enable plugin" checkbox.
 - (Optional) If you have an Aol SID, enter it in the API SID field.
   If you don't already have one, get one at http://www.5minmedia.com/Publishers

USAGE
-----
 1. Browse & Search Videos Relevant to Your Article/Post
   Our library consists of over 450,000 videos, so we have created a few
   options for you to quickly find what you need:
   1.1 Search
    - Search term: enter a search term and hit "search"
    - Video ID: enter a video ID and hit "search"
    - Search for videos within a specific studio or category: refine your search
      by searching in a specific studio or category using the drop-down lists
      on the left
   1.2 Browse Videos
    - Looking for content in a specific category? The plugin allows you to dive
      directly into your vertical or niche and quickly find what’s relevant for
      your audience.
    - Sort the videos by Date or View Count
   1.3 Editors Room Selections
    - Our editorial team is working behind the scenes to surface what’s hot and
      trending in real time. The Editors Room is constantly updated with the
      latest breaking stories and trending topics, making the best videos
      available to you instantly.
    - Enter the editors room, and login to your account
    - In various places you will see "Add to CMS" – once you select this link –
      the video will appear in the plugin under the "My Editors Room Picks".
      Now you can choose if you want to add these videos to your post
   1.4 Videos Semantically Matched to Your Article/Post
    - When you select the "Semantic" tab or the "Automatic Matching" button –
      our VideoSeed semantic engine analyzes the text on your article/post and
      presents only the videos most relevant to this content.
    - We added an indication to let you know the Semantic match
      level: High, Medium or Low.
 2. Select Videos and Add to Your Selections List
   You can add any video to your selections list. This is an interim list that
   you can edit before you actually add the player with the videos to the post:
    - Hover over a video thumbnail to view more details such as: full title,
      restrictions (if there are any), studio name and upload date. A red
      exclamation point warns you that there are restrictions for this video:
      Expiration date or Geo restrictions
    - Click on the video thumbnail to play the video  and view additional
      details on the video
    - Select the + icon to add a video to your playlist
 3. Edit Your Playlist & Insert to the Post/Article
   Whether you are looking for a single video for a post, or a series of videos
   on a given topic, the plugin will provide you with the relevant tool.
    - Select a single video:
      In this case you have 2 options to use in the "Related" videos panel:
        a. Semantically matched
        b. Pre-defined playlist
    - Select a multiple videos playlist:
      a. Add/Remove videos from the list
      b. Reorder the videos
      c. The first video in the list will be the one showing on page load
    - Select "Add to Post"
