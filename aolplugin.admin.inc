<?php
/**
 * @file
 * The backend admin form, which allows the user to enter an SID.
 */

/**
 * Form constructor for the admin form.
 *
 * @ingroup forms
 */
function aolplugin_admin() {
  $form = array();

  $form['aolplugin_sid'] = array(
    '#type' => 'textfield',
    '#title' => t('API SID'),
    '#default_value' => variable_get('aolplugin_sid', AOLPLUGIN_DEFAULT_SID),
    '#size' => 50,
    '#maxlength' => 10,
    '#description' => t("Aol On API SID"),
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['aolplugin_content_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Plugin'),
    '#attributes' => variable_get('aolplugin_content_type', FALSE) ? array("checked" => "checked") : array(),
    '#description' => t("Show the Aol On widget in the node edit forms"),
    '#weight' => 0,
  );

  return system_settings_form($form);
}
